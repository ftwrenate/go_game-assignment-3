#ifndef GO_H
#define GO_H



//stl
#include <map>
#include <memory>
#include <utility>
#include <chrono>
#include <vector>
#include <functional>
#include <set>

namespace mylib {

namespace go {

  class Board;
  struct GameState;
  class Stone;
  class Player;
  class AiPlayer;
  class HumanPlayer;
  class Engine;
  class Boundry;
  class Block;


  using time_type = std::chrono::duration<int,std::milli>;
  constexpr time_type operator"" _ms (unsigned long long int milli) { return time_type(milli); }
  constexpr time_type operator"" _s  (unsigned long long int sec)   { return time_type(sec*1000); }

  enum class StoneColor {
    White         = 0,
    Black       = 1

  };
  enum class PlayerType {
    Human       = 0,
    Ai          = 1
  };

  enum class GameMode {
    VsPlayer    = 0,
    VsAi        = 1,
    Ai          = 2
  };

  using Point   = std::pair<int,int>;
  using BoardData = std::map<Point,Stone>;
  using Size    = std::pair<int,int>;

  class Boundry {

      std::vector<Point> _points;

  };

   class Block {

       std::set<Point> _Stones;
       std::set<Boundry> _Boundries;
   };


  // INVARIANTS:
  //   Board fullfills; what does one need to know in order to consider a given position.
  //   * all stones and their position on the board
  //   * who places the next stone
  //   * was last move a pass move
  //   * meta: blocks and freedoms

  namespace priv {

    class Board_base {
    public:
      using BoardData = std::map<Point,StoneColor>;

      struct Position {
        BoardData     board;
        StoneColor    turn;
        bool          was_previous_pass;

        Position () = default;
        explicit Position ( BoardData&& data, StoneColor turn, bool was_previous_pass);

      }; // END class Position

      // Constructors
      Board_base() = default;
      explicit Board_base( Size size );
      explicit Board_base( BoardData&& data, StoneColor turn, bool was_previous_pass );

      // Board data
      Size             size() const;
      bool             wasPreviousPass() const;
      StoneColor       turn() const;

      // Board
      void             resetBoard( Size size );

      // Board
      Size             _size;
      Position         _current;

    }; // END base class Board_base

    class Stone_base {
    public:
        StoneColor _color;
        Point _point;
        std::shared_ptr<Board> _board;

        Stone_base(StoneColor stColor, go::Point pPosition, std::shared_ptr<Board> shBoard);

        bool hasNorthPosition() const;
        Point NorthPosition() const;

        bool hasSouthPosition() const;
        Point SouthPosition() const;

        bool hasEastPosition() const;
        Point EastPosition() const;

        bool hasWestPosition() const;
        Point WestPosition() const;

    };
 }; // END "private" namespace priv
    class Stone : private priv::Stone_base {
    public:
        Point stonePlace();
        // Using Stone_base constructor
        using Stone_base::Stone_base;
        using Stone_base::hasNorthPosition;
        using Stone_base::hasSouthPosition;
        using Stone_base::hasEastPosition;
        using Stone_base::hasWestPosition;
    };

  class Board : private priv::Board_base {
  public:
    // Enable types
    using Board_base::BoardData;

    using Board_base::Position;

    // Enable constructors
    using Board_base::Board_base;

    // Make visible from Board_base
    using Board_base::resetBoard;
    using Board_base::size;
    using Board_base::wasPreviousPass;
    using Board_base::turn;


    // Validate
    bool                  isNextPositionValid( Point intersection ) const;

    // Actions
    void                  placeStone( Point intersection );
    void                  passTurn();

    // Stones and positions
    bool                  hasStone( Point intersection ) const;
    StoneColor            stone( Point intersection ) const;


  }; // END class Board

  class Engine : public std::enable_shared_from_this<Engine> {
  public:
    Engine();

    void                            newGame( Size size );
    void                            newGameVsAi( Size size );
    void                            newGameAiVsAi( Size size );
    void                            newGameFromState( Board::BoardData&& board, StoneColor turn,
                                                      bool was_previous_pass );

    const std::shared_ptr<Board>    board() const;

    StoneColor                      turn() const;

    const GameMode&                 gameMode() const;
    const std::shared_ptr<Player>   currentPlayer() const;
    void                            placeStone( Point intersection );
    bool                            validateStone( Point intersection ) const;
    void                            passTurn();
    void                            countNumberOfPasses();
    int                             numberOfPasses();
    void                            passTurnNextPlayer();
    void                            nextTurn( time_type think_time = 100_ms );
    bool                            isGameActive() const;


  private:
    std::shared_ptr<Board>    _board;


    GameMode                        _game_mode;
    bool                            _active_game;
    int                             _number_of_passes;

    std::shared_ptr<Player>         _white_player;
    std::shared_ptr<Player>         _black_player;

  }; // END class Engine


} // END namespace go

} // END namespace mylib

#endif //GO_H
