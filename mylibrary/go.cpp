#include "go.h"
#include "randomai.h"


namespace mylib {
namespace go {



  namespace priv {

    Board_base::Board_base( Size size ) {

      resetBoard(size);
    }

    Board_base::Board_base(Board::BoardData&& data, StoneColor turn, bool was_previous_pass)
      : _current{std::forward<Board::BoardData>(data),turn,was_previous_pass}
    {
      // ... init ...
    }

    Board_base::Position::Position(Board::BoardData&& data, StoneColor trn, bool prev_pass)
      : board{data}, turn{trn}, was_previous_pass{prev_pass} {}


    void
    Board_base::resetBoard(Size size) {

      _current.board.clear();
      _size = size;
      _current.turn = StoneColor::Black;
    }

    Size
    Board_base::size() const {

      return _size;
    }

    bool
    Board_base::wasPreviousPass() const {

      return _current.was_previous_pass;
    }

    StoneColor
    Board_base::turn() const {

        return _current.turn == StoneColor::Black ? StoneColor::White : StoneColor::Black;
    }

    Stone_base::Stone_base(StoneColor stColor, Point pPosition, std::shared_ptr<Board> sharedPointBoard)
    {
        _color = stColor;
        _point = pPosition;
        _board = sharedPointBoard;
    }


    bool Stone_base::hasNorthPosition() const
    {
        // Since Im making a method for North we have on the xy-axis: X + 0 and Y + 1
        //
        //             Y-AXIS
        //             North
        //              1
        //               |
        //               |
        //               |
        //               |
        // West -1---------------- 1   East    X-AXIS
        //               |
        //               |
        //               |
        //               |
        //             -1
        //              South

        // Board
        auto boardSize = _board->size();
        // Point on the xy-axis -> x + 0 = 0 and y + 1 = 0
        auto north = Point{_point.first, _point.second-1};

        if(!((north.first <= boardSize.first && north.first >= 1) && (north.second <= boardSize.second && north.second >= 1)))
            return false;
        // Check if the board doesnt have North
        if(!_board->hasStone(north))
            return false;
        return true;
    }

    bool Stone_base::hasSouthPosition() const
    {
        // Since Im making a method for South we have on the xy-axis: X + 0 and Y - 1
        //
        //             Y-AXIS
        //             North
        //              1
        //               |
        //               |
        //               |
        //               |
        // West -1---------------- 1   East    X-AXIS
        //               |
        //               |
        //               |
        //               |
        //             -1
        //              South
        // Board
        auto boardSize = _board->size();
        // Point on the xy-axis -> x + 0 = 0 and y - 1 = 0
        auto south = Point{_point.first, _point.second+1};

        if(!((south.first <= boardSize.first && south.first >= 1) && (south.second <= boardSize.second && south.second >= 1)))
            return false;
        // Check if the board doesnt have South
        if(!_board->hasStone(south))
            return false;
        return true;
    }

    bool Stone_base::hasEastPosition() const
    {
        // Since Im making a method for East we have on the xy-axis: X - 1 and Y + 0
        //
        //             Y-AXIS
        //             North
        //              1
        //               |
        //               |
        //               |
        //               |
        // West -1---------------- 1   East    X-AXIS
        //               |
        //               |
        //               |
        //               |
        //             -1
        //              South
        // Board
        auto boardSize = _board->size();
        // Point on the xy-axis -> x - 1 = 0 and y + 0 = 0
        auto east = Point{_point.first+1, _point.second};

        if(!((east.first <= boardSize.first && east.first >= 1) && (east.second <= boardSize.second && east.second >= 1)))
            return false;
        // Check if the board doesnt have East
        if(!_board->hasStone(east))
            return false;
        return true;
    }

    bool Stone_base::hasWestPosition() const
    {
        // Since Im making a method for West we have on the xy-axis: X + 1 and Y + 0
        //
        //             Y-AXIS
        //             North
        //              1
        //               |
        //               |
        //               |
        //               |
        // West -1---------------- 1   East    X-AXIS
        //               |
        //               |
        //               |
        //               |
        //             -1
        //              South
        // Board
        auto boardSize = _board->size();
        // Point on the xy-axis -> x + 1 = 0 and y + 0 = 0
        auto west = Point{_point.first-1, _point.second};

        if(!((west.first <= boardSize.first && west.first >= 1) && (west.second <= boardSize.second && west.second >= 1)))
            return false;
        // Check if the board doesnt have West
        if(!_board->hasStone(west))
            return false;
        return true;
    }

  }

  void
  Board::placeStone(Point intersection) {

    _current.board[intersection] = turn();
    _current.turn = turn();

  }

  void
  Board::passTurn() {

    _current.turn = turn();
  }

  bool
  Board::hasStone(Point intersection) const {

    return _current.board.count(intersection);
  }

  StoneColor
  Board::stone(Point intersection) const {

    return _current.board.at(intersection);
  }

  // Rule: Cannot put stones over eachother.
  bool
  Board::isNextPositionValid(Point intersection) const {

    return !Board::hasStone(intersection);
  }
  // Rule: The pass rule
  void
  Engine::countNumberOfPasses()
  {
      _number_of_passes++;

  }

  int Engine::numberOfPasses()
  {
      return _number_of_passes;
  }

  void Engine::passTurnNextPlayer()
  {
     return  _board->passTurn();
  }

  Engine::Engine()
    : _board{std::make_shared<Board>()}, _game_mode{}, _active_game{false},
      _number_of_passes(0),
      _white_player{nullptr}, _black_player{nullptr} {}

  void
  Engine::newGame(Size size) {

    _board->resetBoard(size);

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameVsAi(Size size) {

    _board->resetBoard(size);

    _game_mode = GameMode::VsAi;
    _active_game = true;

    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),   StoneColor::White);
  }

  void
  Engine::newGameAiVsAi(Size size) {

    _board->resetBoard(size);

    _game_mode = GameMode::Ai;
    _active_game = true;

    _white_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameFromState(Board::BoardData&& board, StoneColor turn, bool was_previous_pass) {

    _board = std::make_shared<Board> (std::forward<Board::BoardData>(board),turn,was_previous_pass);

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  const std::shared_ptr<Board> Engine::board() const {

    return _board;
  }

  const GameMode&
  Engine::gameMode() const {

    return _game_mode;
  }

  StoneColor Engine::turn() const {

    return _board->turn();
  }

  const std::shared_ptr<Player>
  Engine::currentPlayer() const {

    if(      turn() == StoneColor::Black ) return _black_player;
    else if( turn() == StoneColor::White ) return _white_player;
    else                              return nullptr;
  }

  void
  Engine::placeStone(Point intersection) {

    _board->placeStone(intersection);
  }

  void
  Engine::passTurn() {

    if(board()->wasPreviousPass()) {
      _active_game = false;
      return;
    }

    _board->passTurn();
  }

  void
  Engine::nextTurn(std::chrono::duration<int,std::milli> think_time) {

    if( currentPlayer()->type() != PlayerType::Ai) {

        return;
    }
    auto p = std::static_pointer_cast<AiPlayer>(currentPlayer());

    p->think( think_time );
    if( p->nextMove() == AiPlayer::Move::PlaceStone )
      placeStone( p->nextStone() );
    else
      passTurn();
  }
  bool
  Engine::isGameActive() const {

    return _active_game;
  }

  bool
  Engine::validateStone(Point pos) const {

      return _board->isNextPositionValid(pos);
  }

  Point Stone::stonePlace()
  {
      return _point;
  }

} // END namespace go
} // END namespace mylib
