
// My Go Engine
#include "../../mylibrary/go.h"

// gtest
#include <gtest/gtest.h>

namespace go = mylib::go;

TEST(GoEngineTest_Board,FromStateConstructor) {

//      |x x x x x|
//      |x x x x x|
//      |x x b x x|
//      |x x x x x|
//      |x x x x x|

  go::Board::BoardData board_data;
  board_data[go::Point(3,3)] = go::StoneColor::Black;

  // Create a engine from state
  go::Board board {std::move(board_data),go::StoneColor::White,false};

  // EXPECT a stone at (3,3)
  EXPECT_TRUE(board.hasStone({3,3}));

  // EXPECT a black stone at (3,3)
  EXPECT_TRUE(board.stone({3,3}) == go::StoneColor::Black);
}
//Check if intersection between black and white stones
TEST(GoEngineTest_Board, Occupied) {
//          North
//      |x x x  x x|
//      |x x x  x x|
//  West|x x BW x x|East
//      |x x x  x x|
//      |x x x  x x|
//          South

      // Black stone at point (3,3)
      go::Board::BoardData board_data;
      board_data[go::Point(3,3)] = go::StoneColor::Black;

      // Create a engine from state
      go::Board board {std::move(board_data),go::StoneColor::White,false};

      // EXPECT_FALSE: A white stone on point (3,3) (intersection)
      EXPECT_FALSE(board.isNextPositionValid(go::Point(3,3)));
}
// Check the North position
TEST(GoEngineTest_Board, PositionNorth) {
//         North
//      |x x x x x|
//      |x x W x x|
//  West|x x B x x|East
//      |x x x x x|
//      |x x x x x|
//         South


  go::StoneColor W = go::StoneColor::White;
  // Create a 5x5 board
  auto board = std::make_shared<go::Board>(go::Size(5,5));
  // White position: (3,2)
  auto NorthPosition = std::make_shared<go::Stone>(W, go::Point(3,2), board);
  // Black position: (3,3)
  auto SouthPosition = std::make_shared<go::Stone>(W, go::Point(3,3), board);
  // Place stones
  board->placeStone(NorthPosition->stonePlace());
  board->placeStone(SouthPosition->stonePlace());

  // A white stone is at the NorthPosition of the black stone
  EXPECT_TRUE(SouthPosition->hasNorthPosition());
  // No white stone at the SouthPosition of the black stone
  EXPECT_FALSE(SouthPosition->hasSouthPosition());
  // No white stone at the EastPosition of the black stone
  EXPECT_FALSE(SouthPosition->hasEastPosition());
  // No white stone at the WestPosition of the black stone
  EXPECT_FALSE(SouthPosition->hasWestPosition());
}
// Check the South position
TEST(GoEngineTest_Board, PositionSouth) {
//         North
//      |x x x x x|
//      |x x x x x|
//  West|x x B x x|East
//      |x x W x x|
//      |x x x x x|
//         South

  go::StoneColor W = go::StoneColor::White;
  // Create a 5x5 board
  auto board = std::make_shared<go::Board>(go::Size(5,5));
  // White position: (3,4)
  auto SouthPosition = std::make_shared<go::Stone>(W, go::Point(3,4), board);
  // Black position: (3,3)
  auto NorthPosition = std::make_shared<go::Stone>(W, go::Point(3,3), board);
  // Place stones
  board->placeStone(NorthPosition->stonePlace());
  board->placeStone(SouthPosition->stonePlace());

  // A white stone is at the SouthPosition of the black stone
  EXPECT_TRUE(NorthPosition->hasSouthPosition());
  // No white stone at the NorthPosition of the black stone
  EXPECT_FALSE(NorthPosition->hasNorthPosition());
  // No white stone at the EastPosition of the black stone
  EXPECT_FALSE(NorthPosition->hasEastPosition());
  // No white stone at the WestPosition of the black stone
  EXPECT_FALSE(NorthPosition->hasWestPosition());
}

// Check the East position
TEST(GoEngineTest_Board, PositionEast) {
//         North
//      |x x x x x|
//      |x x x x x|
//  West|x x B W x|East
//      |x x x x x|
//      |x x x x x|
//         South

  go::StoneColor W = go::StoneColor::White;
  // Create a 5x5 board
  auto board = std::make_shared<go::Board>(go::Size(5,5));
  // White position: (4,3)
  auto EastPosition = std::make_shared<go::Stone>(W, go::Point(4,3), board);
  // Black position: (3,3)
  auto WestPosition = std::make_shared<go::Stone>(W, go::Point(3,3), board);
  // Place stones
  board->placeStone(EastPosition->stonePlace());
  board->placeStone(WestPosition->stonePlace());

  // A white stone is at the EastPosition of the black stone
  EXPECT_TRUE(WestPosition->hasEastPosition());
  // No white stone at the NorthPosition of the black stone
  EXPECT_FALSE(WestPosition->hasNorthPosition());
  // No white stone at the WestPosition of the black stone
  EXPECT_FALSE(WestPosition->hasWestPosition());
  // No white stone at the SouthPosition of the black stone
  EXPECT_FALSE(WestPosition->hasSouthPosition());
}

// Check the West position
TEST(GoEngineTest_Board, PositionWest) {
//         North
//      |x x x x x|
//      |x x x x x|
//  West|x W B x x|East
//      |x x x x x|
//      |x x x x x|
//         South

  go::StoneColor W = go::StoneColor::White;
  // Create a 5x5 board
  auto board = std::make_shared<go::Board>(go::Size(5,5));
  // White position: (2,3)
  auto WestPosition = std::make_shared<go::Stone>(W, go::Point(2,3), board);
  // Black position: (3,3)
  auto EastPosition = std::make_shared<go::Stone>(W, go::Point(3,3), board);
  // Place stones
  board->placeStone(EastPosition->stonePlace());
  board->placeStone(WestPosition->stonePlace());

  // A white stone is at the WestPosition of the black stone
  EXPECT_TRUE(EastPosition->hasWestPosition());
  // No white stone at the NorthPosition of the black stone
  EXPECT_FALSE(EastPosition->hasNorthPosition());
  // No white stone at the EastPosition of the black stone
  EXPECT_FALSE(EastPosition->hasEastPosition());
  // No white stone at the SouthPosition of the black stone
  EXPECT_FALSE(EastPosition->hasSouthPosition());
}
TEST(GoEngineTest_LegalStonePlacements,SimpleKoRuleTest) {
//         North
//      |x x x x x|
//      |x x W x x|
//  West|x W B W x|East
//      |x B x B x|
//      |x x B x x|
//         South

  go::StoneColor B = go::StoneColor::Black;
  go::StoneColor W = go::StoneColor::White;

  go::Board::BoardData b;
                b[{3,2}] = W;
  b[{2,3}] = W; b[{3,3}] = B; b[{4,3}] = W;
  b[{2,4}] = B;               b[{4,4}] = B;
                b[{3,5}] = B;


  // Create a PvP engine
  auto engine = std::make_shared<go::Engine>();
  engine->newGameFromState( std::move(b), go::StoneColor::White, false );

  // Set up the game for the 'ko' rule to trigger:
  // white removes the freedoms of black's Stone on (3,3)
  // and captures it.
  engine->placeStone(go::Point(3,4));

  // Play a illegal move for black triggering the 'ko' rule
  // when black tries to capture whites (3,4).
  EXPECT_FALSE(engine->validateStone(go::Point(3,4)));
}
